<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "breakdown".
 *
 * @property int $id
 * @property string $title
 * @property int $levelId
 * @property int $statusId
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'levelId', 'statusId'], 'required'],
            [['levelId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'levelId' => 'Level ID',
            'statusId' => 'Status ID',
        ];
    }
	/*public function beforeSave($insert)///another way to make "open" automaticly"
	{
		$return = parent::beforeSave($insert);
		
		if ($this->isNewRecord)
		    $this->statusId = 1;
		
		return $return;
	}
	*/
		
	public function getFindLevel()
    {
		return  Level::findOne($this->levelId);
    }
	
	public function getFindStatus()
    {
		return  Status::findOne($this->statusId);
    }

}
