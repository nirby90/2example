<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m170719_083924_create_breakdown_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('breakdown', [
            'id' => $this->primaryKey(),
			'title' => $this->string()->notNull(),
			'levelId' => $this->integer()->notNull(),
			'statusId' => $this->integer()->notNull(),
			
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('breakdown');
    }
}
