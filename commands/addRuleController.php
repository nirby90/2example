<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{
	public function actionOwnUpdateuserRule()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnUpdateuserRule;
		$auth->add($rule);
	}
	public function actionOwnViewurgentRule()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnViewurgentRule;
		$auth->add($rule);
	}
}
