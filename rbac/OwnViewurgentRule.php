<?php 
namespace app\rbac;

use yii\rbac\Rule;
use app\models\User;
use app\models\Breakdown;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;

	/**
	 * Checks if authorID matches user passed via params
	 */
	class OwnViewurgentRule extends Rule
	{
		public $name = 'OwnViewurgentRule';
		
		/**
		 * @param string|int $user the user ID.
		 * @param Item $item the role or permission that this rule is associated with
		 * @param array $params parameters passed to ManagerInterface::checkAccess().
		 * @return bool a value indicating whether the rule permits the role or permission it is associated with.
		 */
		public function execute($user, $item, $params)
		{	
			if(isset($user)){
				$currentUserRole = \Yii::$app->authManager->getRolesByUser($user);
				
				if($currentUserRole == 'manager')
					return false;
					
			}
		
			return false;
		}
	}
	
?>