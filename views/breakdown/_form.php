<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Level;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'levelId')->textInput() ?-->

    <!--?= $form->field($model, 'statusId')->textInput() ?-->
	
	<!--?= $form->field($model, 'statusId')->dropDownList(Status::getStatusId()) ?-->
	
	<?php if($model->isNewRecord){ ?>
			<div style="display:none;"> <?= $form->field($model, 'statusId')->textInput(['value'=>1]) ?> </div>
		<?php }else{ ?>
			<?= $form->field($model, 'statusId')-> dropDownList(Status::getStatusId()) ?>
	<?php } ?>
	
	<?= $form->field($model, 'levelId')->dropDownList(Level::getLevelId()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
